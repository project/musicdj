musicdj.theme

-- Information --

	Author:  Jason Lee [jase951] (http://jason.ilobby.co.uk)

		 Requires the PHPTemplate theme engine.
		 Released for Drupal 4.7.x
		 Tested on Drupal 4.7.6
		 This theme has a fixed width and is all left aligned.

	Contact: Please, see my website and use the forum or contact me directly via my contact form.


-- INITIAL INSTALL --

You may have to go to [?q=/admin/block] page to enable the navigation block for the sidebar.


-- BANNER FEATURE --

A banner feature is available and has been packaged with this theme.  Please, read the INSTALL.txt within the banner folder[/images/banner/INSTALL.txt]


-- CHANGELOG.txt --

The CHANGELOG.txt file contains a complete list of changes that have been made.