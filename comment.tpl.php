<?php
?>
<div class="commentbox<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; ?>">
<?php if ($comment->new) : ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
<?php endif; ?>
    <?php if ($picture) { ?>
      <div class="right"><?php print $picture ?></div>
    <?php }; ?>
	<div class="comment-title"><?php print $title ?></div>
	<?php print $content ?>
</div>
<div class="commentfooter"><?php print $author ?> | <?php print $date ?></div>
