<?php
?>
<div class="post<?php print ($sticky) ? " sticky" : ""; ?>">
  <?php if ($page == 0) { ?>
    <h1><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h1>
  <?php } elseif ($picture) { ?>
    <div class="left"><?php print $picture ?></div>
  <?php }; ?>
  <?php print $content ?>
  <?php if ($links) { ?>
    <h4><?php if ($terms) { print t("Posted in") . ' ' . $terms; }; ?>
        <?php if ($submitted) { ?>
          <?php print $submitted ?><br />
        <?php }; ?>
        <?php print $links ?></h4>
  <?php } else { ?>
    <div class="clearfix"><br /></div>
  <?php }; ?>
</div>