<?php
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN"
  "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>">
<head>
<title><?php print $head_title ?></title>
<meta http-equiv="Content-Style-Type" content="text/css" />
<?php print $head ?>
<style type="text/css" media="screen">
@import "<?php print base_path(). path_to_theme(); ?>/style.css";
</style>
<style type="text/css" media="print">
@import "<?php print base_path(). path_to_theme(); ?>/print.css";
</style>
<?php print $styles ?>
<style type="text/css">
 div.c1 {80%;}
</style>
</head>
<body>
<!-- CONTENT WRAPPER -->
<div id="globalwrap">
  <!-- TOP HEADER BEGIN -->
  <div id="header">
    <!-- HEADER WRAP BEGIN -->
    <div id="header-wrap">
      <div class="header-l">
        <?php if ($logo) { ?>
        <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
        <?php } else if ($site_name) { ?>
        <h1 class='site-name'><a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><?php print $site_name ?></a></h1>
        <?php } ?>
        <?php if ($site_slogan) { ?>
        <div class='site-slogan'> <?php print $site_slogan ?> </div>
        <?php } ?>
      </div>
      <div class="header-r">
        <?php if ($primary_links) { ?>
        <div id="bar"> <?php print theme('primary', $primary_links) ?> </div>
        <!--end bar-->
        <?php } ?>
        <?php if ($search_box) { ?>
        <div id="search_box">
          <p><?php print t('Search:') ?></p>
          <?php print $search_box ?> </div>
        <?php } ?>
      </div>
      <!-- HEADER WRAP END -->
    </div>
    <?php if ($header != "") { ?>
    <div id="head"> <?php print $header ?> </div>
    <?php }; ?>
  </div>
  <!-- TOP HEADER END -->
  <?php if ($banner != "") { ?>
  <div id="banner"> <?php print $banner ?> </div>
  <?php }; ?>
  <!-- MAIN WRAPPER -->
  <div id="wrapper">
    <!-- LEFT SIDE BEGIN -->
    <div id="side-a">
      <?php $wider_screen = array('block','settings','themes'); // added for more admin usability, add more as needed ?>
      <!-- <div id="content" <?php if (in_array(arg(1),$wider_screen)) { ?> style=""<?php } ?>> -->
      <!-- PRIMARY BEGIN -->
      <div id="primary">
        <div id="wrappermain">
          <?php if ($primary_header != "") { ?>
          <div id="headermain"> <?php print $primary_header ?> </div>
          <?php }; ?>
          <!-- PRIMARY CONTAINER BEGIN -->
          <div id="container">
            <!-- PRIMARY LEFT SIDE BEGIN -->
            <div id="side-a-main" <?php if ($central != "") { ?> class="c1" <?php }; ?>>
              <!-- MAIN CONTENT GOES HERE -->
              <?php print $breadcrumb ?>
              <?php if ($mission) { ?>
              <div id="mission"> <?php print $mission ?> </div>
              <?php } ?>
              <?php print $help ?><?php print $messages ?>
              <?php if ($title) { ?>
              <h1 class="title"><?php print $title ?></h1>
              <?php } ?>
              <div class="tabs"> <?php print $tabs ?> </div>
              <?php print $content; ?>
              <!-- MAIN CONTENT END -->
              <!-- PRIMARY LEFT SIDE END -->
            </div>
            <?php if ($central != "") { ?>
            <!-- PRIMARY CENTRAL BEGIN -->
            <div id="side-b-main"> <?php print $central ?>
              <!-- PRIMARY CENTRAL END -->
            </div>
            <?php }; ?>
            <!-- PRIMARY LEFT SIDE END -->
          </div>
          <?php if ($primary_footer != "") { ?>
          <!-- PRIMARY FOOTER BEGIN -->
          <div id="footermain"> <?php print $primary_footer ?>
            <!-- PRIMARY FOOTER END -->
          </div>
          <?php }; ?>
          <!-- PRIMARY MAIN WRAPPER END -->
        </div>
        <!-- PRIMARY END -->
      </div>
      <!-- LEFT SIDE END -->
    </div>
    <!-- RIGHT SIDE BEGIN -->
    <div id="side-b">
      <!-- SECONDARY BEGIN -->
      <div id="secondary">
        <?php if ($sidebar != "") { ?>
        <?php print $sidebar ?>
        <?php }; ?>
        <!-- SECONDARY END -->
      </div>
    </div>
    <!-- RIGHT SIDE ENDS -->
  </div>
  <!-- MAIN WRAPPER ENDS -->
  <!-- FOOTER BEGINS -->
  <div class="footer" id="footer">
    <?php if (isset($secondary_links)) { ?>
      <?php print theme('links', $secondary_links) ?>
      <?php } ?>
    <?php print $footer_message ?>
    <?php if ($footer_message) { print "<br /><br />"; } ?>
    Theme by <a href="http://jason.ilobby.co.uk">Jason Lee</a> and <a href="http://imrgoodbuy.co.uk">I Missed a Good Buy</a></div>
  <?php print $closure ?>
  <!-- FOOTER ENDS -->
</div>
<!-- ALL CONTENT ENDS -->
</body>
</html>
