<?php

function musicdj_primary($items = array()) {
  $url = 'http';
  if ($_SERVER['HTTPS']=='on')
    $url .=  's';
  $url .=  '://';
  if ($_SERVER['SERVER_PORT']!='80')
    $url .=  $_SERVER['HTTP_HOST'].':'.$_SERVER['SERVER_PORT'].base_path();
  else
    $url .=  $_SERVER['HTTP_HOST'].base_path();
  if ($_SERVER['QUERY_STRING']>' ')
    $url .=  '?'.$_SERVER['QUERY_STRING'];

  $output = '<ul>';
  if (!empty($items)) {
    foreach ($items as $item) {
      preg_match("/<a\s*.*?href\s*=\s*['\"]([^\"'>]*).*?>(.*?)<\/a>/i", $item, $matches);
      if ($url == $matches[1]) {
        $output .= '<li class="active">'. $item .'</li>';
      } else {
        $output .= '<li>'. $item .'</li>';
      }
    }
  }
  $output .= '</ul>';
  return $output;
}

function musicdj_regions() {
  return array(
  	   'banner' => t('banner'),
       'sidebar' => t('sidebar'),
       'primary_header' => t('primary header'),
       'header' => t('header'),
       'content' => t('content'),
       'footer' => t('footer'),
       'central' => t('central'),
       'primary_footer' => t('primary footer'),
  );
}

function _phptemplate_variables($hook, $vars = array()) {
  if ($vars['logo'] == base_path() . path_to_theme() .'/logo.png') {
    $vars['logo'] = base_path() . path_to_theme() .'/logo.png';
  }
 
  return $vars;
}

function phptemplate_profile_listing($account, $fields = array()) {

  $output  = "<div class=\"profile clearfix\">\n";
  $output .= theme('user_picture', $account);
  $output .= ' <div class="name">'. theme('username', $account) ."</div>\n";

  foreach ($fields as $field) {
    if ($field->value) {
      $output .= " <div class=\"field\">$field->value</div>\n";
    }
  }

  $output .= "</div>\n";

  return $output;
}

function phptemplate_system_themes($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['description'])) {
      $row[] = form_render($form[$key]['description']) . form_render($form[$key]['screenshot']) ;
      $row[] = array('data' => form_render($form['status'][$key]), 'align' => 'center');
      if ($form['theme_default']) {
        $row[] = array('data' => form_render($form['theme_default'][$key]) . form_render($form[$key]['operations']), 'align' => 'center');
      }
    }
    $rows[] = $row;
  }
  $header = array(t('Name/Screenshot'), t('Enabled'), t('Default'));
  $output = theme('table', $header, $rows);
  $output .= form_render($form);
  return $output;
}

function phptemplate_system_user($form) {
  foreach (element_children($form) as $key) {
    $row = array();
    if (is_array($form[$key]['description'])) {
      $row[] = form_render($form[$key]['description']) . form_render($form[$key]['screenshot']);
      $row[] = array('data' => form_render($form['theme'][$key]), 'align' => 'center');
    }
    $rows[] = $row;
  }

  $header = array(t('Name/Screenshot'), t('Selected'));
  $output = theme('table', $header, $rows);
  return $output;
}
?>